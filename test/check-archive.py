#! /usr/bin/python3

import sys

if sys.version_info < (3,):
    sys.exit("benchexec.test_tool_info needs Python 3 to run.")

import argparse
import os
import re
import tempfile
import xml.etree.ElementTree as ET
import zipfile
import logging
import benchexec
from benchexec import model
from benchexec.tools.template import BaseTool2
from subprocess import call
from types import SimpleNamespace
from urllib.request import urlopen, Request, HTTPError
from pathlib import Path
from enum import Enum
import _util as util

sys.dont_write_bytecode = True  # prevent creation of .pyc files

SUCCESS = 0
ERROR = 1

TEST_COMP_BENCHMARK_DEF_TEMPLATE = (
    "https://gitlab.com/sosy-lab/test-comp/bench-defs/raw/main/benchmark-defs/%s.xml"
)
TEST_COMP_DEF_ERR = "file '%s' not available. Please rename the archive to match an existing benchmark definition, or add a new benchmark definition at 'https://gitlab.com/sosy-lab/test-comp/bench-defs'."

SV_COMP_BENCHMARK_DEF_TEMPLATE = (
    "https://gitlab.com/sosy-lab/sv-comp/bench-defs/-/raw/main/benchmark-defs/%s.xml"
)
SV_COMP_DEF_ERR = "file '%s' not available. Please rename the archive to match an existing benchmark definition, or add a new benchmark definition at 'https://gitlab.com/sosy-lab/sv-comp/bench-defs'."


class ZIPFileCode(Enum):
    # define some constants for zipfiles,
    # needed to get the interesting bits from zipped objects, for further details take a look at
    # https://unix.stackexchange.com/questions/14705/the-zip-formats-external-file-attribute/14727#14727
    S_IFIFO = 0o010000  # named pipe (fifo)
    S_IFCHR = 0o020000  # character special
    S_IFDIR = 0o040000  # directory
    S_IFBLK = 0o060000  # block special
    S_IFREG = 0o100000  # regular
    S_IFLNK = 0o120000  # symbolic link
    S_IFSOCK = 0o140000  # socket


def is_flag_set(attr, flag: ZIPFileCode):
    """returns whether a flag is set or not"""
    return (attr & (flag << 16)) == (flag << 16)


def get_attributes(info_object):
    return {
        "named pipe": is_flag_set(info_object.external_attr, ZIPFileCode.S_IFIFO.value),
        "special char": is_flag_set(
            info_object.external_attr, ZIPFileCode.S_IFCHR.value
        ),
        "directory": is_flag_set(info_object.external_attr, ZIPFileCode.S_IFDIR.value),
        "block special": is_flag_set(
            info_object.external_attr, ZIPFileCode.S_IFBLK.value
        ),
        "regular": is_flag_set(info_object.external_attr, ZIPFileCode.S_IFREG.value),
        "symbolic link": is_flag_set(
            info_object.external_attr, ZIPFileCode.S_IFLNK.value
        ),
        "socket": is_flag_set(info_object.external_attr, ZIPFileCode.S_IFSOCK.value),
    }


def error(arg, cause=None, label="    ERROR", exit_on_first_error=False):
    util.error(arg, cause, label)
    if exit_on_first_error:
        sys.exit(1)


def info(msg, label="INFO"):
    util.info(msg, label)


def _contains_file_in_root(root_directory: str, name_list: list, check_against: str):
    for file_root in name_list:
        file_path = os.path.join(root_directory, check_against)
        # check if there is a file rootDirectory/check_against. Second check ensures that it is a file (only one /)
        if (
            file_root.lower().startswith(file_path.lower())
            and file_root.count("/") == 1
        ):
            return True

    return False


def check_zipfile(tool, competition_track, exit_on_first_error=False):
    zip_filename = (
        args.archives_root
        / f"{tool}-{util.get_track_for_filename(competition_track)}.zip"
    )

    assert os.path.isfile(zip_filename)
    try:
        zip_content = zipfile.ZipFile(zip_filename)
    except zipfile.BadZipfile as e:
        error("zipfile is invalid", cause=e, exit_on_first_error=exit_on_first_error)
        return ERROR
    namelist = zip_content.namelist()
    if not namelist:
        error("zipfile is empty", exit_on_first_error=exit_on_first_error)
        return ERROR

    # check whether there is a single root directory for all files.
    root_directory = namelist[0].split("/")[0] + "/"
    status = SUCCESS
    for name in namelist:
        if not name.startswith(root_directory):
            error(
                "file '{}' is not located under a common root directory".format(name),
                exit_on_first_error=exit_on_first_error,
            )
            status = ERROR

    # check if root directory contains readme
    if not _contains_file_in_root(root_directory, namelist, "readme"):
        error(
            f"no readme found in root directory: {root_directory}",
            exit_on_first_error=exit_on_first_error,
        )
        status = ERROR

    # check if root directory contains license
    if not _contains_file_in_root(
        root_directory, namelist, "license"
    ) and not _contains_file_in_root(root_directory, namelist, "license"):
        error(
            f"no license found in root directory: {root_directory}",
            exit_on_first_error=exit_on_first_error,
        )
        status = ERROR

    # check whether there are unwanted files
    pattern = re.compile(
        ".*(\/\.git\/|\/\.svn\/|\/\.hg\/|\/CVS\/|\/__MACOSX|\/\.aptrelease).*"
    )
    for name in namelist:
        if pattern.match(name):
            error(
                "file '{}' should not be part of the zipfile".format(name),
                exit_on_first_error=exit_on_first_error,
            )
            status = ERROR

    # check whether all symlinks point to valid targets
    directories = set(os.path.dirname(f) for f in namelist)
    for info_object in zip_content.infolist():
        attr = get_attributes(info_object)
        if attr["symbolic link"]:
            relativTarget = bytes.decode(zip_content.open(info_object).read())
            target = os.path.normpath(
                os.path.join(os.path.dirname(info_object.filename), relativTarget)
            )
            if not target in directories and not target in namelist:
                error(
                    "symbolic link '{}' points to invalid target '{}'".format(
                        info_object.filename, target
                    ),
                    exit_on_first_error=exit_on_first_error,
                )
                status = ERROR

    return status, root_directory


def check_benchmark_file(tool: str, competition_track: str, err: str, template: str):
    # check that a benchmark definition exists for this tool in the official repository
    benchmark_def_name = util.get_benchmark_filename(tool, competition_track)
    benchmark_url = template % benchmark_def_name
    try:
        r = Request(benchmark_url, headers={"User-Agent": "Mozilla/5.0"})
        request = urlopen(r)
    except HTTPError:
        error(err % benchmark_url)
        return ERROR
    content = request.read()
    benchmark_definition = ET.fromstring(content)
    # Test access of something in XML structure
    tool_name = benchmark_definition.get("tool")
    return SUCCESS, tool_name


def check_tool_info_module(
    tool: str, competition_track: str, root_directory: Path, tool_name: str, config: str
):
    zip_filename = (
        args.archives_root
        / f"{tool}-{util.get_track_for_filename(competition_track)}.zip"
    )
    with tempfile.TemporaryDirectory(prefix="comp_check_") as tmp_dir:
        # lets use the real unzip, because Python may not handle symlinks
        call(["unzip", "-q", "-d", tmp_dir, zip_filename])

        tool_dir = os.path.join(tmp_dir, root_directory)
        try:
            os.chdir(tool_dir)
            return _check_tool_info_module(tool_name, config)
        finally:
            os.chdir(os.environ["PWD"])


def _check_tool_info_module(tool_name, config):
    status = SUCCESS
    try:
        # nice colorful dump, but we would need to parse it
        # from benchexec import test_tool_info
        # test_tool_info.print_tool_info(toolname)

        _, tool = model.load_tool_info(tool_name, config)
    except (Exception, SystemExit) as e:
        error(f"loading tool-info for {tool_name} failed", cause=e)
        return ERROR

    try:
        # import inspect
        # if not inspect.getdoc(tool):
        #     error("tool %s has no documentation" % toolname)
        exe = tool.executable(BaseTool2.ToolLocator(use_path=True, use_current=True))
        if not exe:
            error("tool '%s' has no executable" % tool_name)
            status = ERROR
        if not os.path.isfile(exe) or not os.access(exe, os.X_OK):
            error("tool '%s' with file %s is not executable" % (tool_name, exe))
            status = ERROR
        if exe:
            status |= _checks_on_executable(tool, exe, tool_name)
    except Exception as e:
        error(f"querying tool executable failed for {tool_name}", cause=e)
        status = ERROR

    return status


def _checks_on_executable(tool, exe, toolname):
    status = SUCCESS
    try:
        reported_name = tool.name()
        if not reported_name:
            error("tool '%s' has no name" % toolname)
            status = ERROR
    except Exception as e:
        error(f"querying tool-name failed for {toolname}", cause=e)
        status = ERROR
        reported_name = ""
    if not reported_name:
        reported_name = ""

    try:
        version = tool.version(exe)
        if not version:
            error("tool '%s' has no version number" % toolname)
            status = ERROR
        if "\n" in version:
            error(
                "tool '%s' has an invalid version number (newline in version)"
                % toolname
            )
            status = ERROR
        if len(version) > 100:  # long versions look ugly in tables
            error("tool '%s' has a very long version number" % toolname)
            status = ERROR
        if version.startswith(reported_name):
            error(
                "tool '%s' is part of its own version number '%s'" % (toolname, version)
            )
            status = ERROR
    except Exception as e:
        error(f"querying tool version failed for {toolname}", cause=e)
        status = ERROR
        version = ""
    if not version:
        version = ""

    try:
        programFiles = list(tool.program_files(exe))
    except Exception as e:
        error(f"querying program files failed for {toolname}", cause=e)
        status = ERROR

    label, displayed_name = "     --> ", reported_name + " " + version
    if exe and version:
        info(displayed_name, label=label)
    else:
        error(displayed_name, label=label)
        status = ERROR
    return status


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Check archive for a given competition."
    )
    parser.add_argument(
        "--exit-on-first-error",
        action="store_true",
        default=False,
        help="Exit on first error.",
    )
    subparsers = parser.add_subparsers(
        title="competition", dest="competition", help="Available commands"
    )
    parser.add_argument(
        "--archives-root",
        type=Path,
        help="Path to archives directory",
    )
    parser.add_argument(
        "--competition-track",
        type=str,
        help="Competition track",
    )
    sv_comp_parser = subparsers.add_parser(
        "SV-COMP", help="Check whether the tool's archive matches the SV-COMP format."
    )
    sv_comp_parser.add_argument("tool", type=Path, help="Tool name")
    test_comp_parser = subparsers.add_parser(
        "Test-Comp",
        help="Check whether the tool's archive matches the Test-Comp format.",
    )
    test_comp_parser.add_argument("tool", type=Path, help="Tool name")
    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format=None)
    args = parse_arguments()
    exit_on_first_error = args.exit_on_first_error
    SV_COMP = "SV-COMP"
    err = SV_COMP_DEF_ERR if args.competition == SV_COMP else TEST_COMP_DEF_ERR
    template = (
        SV_COMP_BENCHMARK_DEF_TEMPLATE
        if args.competition == SV_COMP
        else TEST_COMP_BENCHMARK_DEF_TEMPLATE
    )

    logging.info(
        f"This script checks the archive for tool '{args.tool}' for '{args.competition}' and '{args.competition_track} ' with\n\tPython {sys.version}\n\tand BenchExec {benchexec.__version__}"
    )

    # dummy config. this script is meant to be executed by the CI,
    # so no need to run it in an extra container:
    config = SimpleNamespace()
    config.container = False

    file_status, root_directory = check_zipfile(
        args.tool, args.competition_track, exit_on_first_error
    )
    def_status, tool_name = check_benchmark_file(
        args.tool, args.competition_track, err, template
    )
    module_status = check_tool_info_module(
        args.tool, args.competition_track, root_directory, tool_name, config
    )
    sys.exit(file_status | def_status | module_status)
