#!/bin/bash

source "$(dirname "$0")"/../configure.sh

echo "----------------------------------------------"
echo "Pulling new archive DOIs."
(cd fm-tools/; git pull --rebase --prune)

echo "----------------------------------------------"
echo "Pulling new archives and add new jobs to the queue."
"$SCRIPT_DIR"/execute_runs/list-tools.sh "$COMPETITIONNAME" "$YEAR" "$TRACK" | while read -r JOB; do
  VERIFIER="archives/$JOB-$PRODUCER.zip"
  echo ""
  echo "Considering $JOB"

  RUNS=$(find results-verified/ -maxdepth 1 -name "$JOB.*.logfiles.zip")
  if [[ $RUNS != "" ]]; then
    # Look up the most recent run result.
    RESULT=$(ls -t $RUNS | head -1)
    RESULT_TIME=${RESULT##*$JOB.}
    RESULT_TIME=${RESULT_TIME%.logfiles.zip}
    ARCHIVE_TIME=$(date "+%Y-%m-%d_%H-%M-%S" --reference="$VERIFIER")
    echo "Time stamp of run-result: $RESULT_TIME"
    echo "Time stamp of archive:    $ARCHIVE_TIME"
    if [[ "$ARCHIVE_TIME" < "$RESULT_TIME" ]]; then
      echo "Job was already processed."
      continue
    fi
  fi
  # Add job only if it was not already considered before.
  if [[ -e "queue/$JOB"  ||  -e "queue/$JOB.wait"  ||  -e "queue/$JOB.running"  ||  -e "queue/$JOB.finished" ]] ; then
    echo "Not scheduled $JOB"
    continue
  fi
  echo "Scheduling $JOB"
  touch --reference="$VERIFIER" "queue/$JOB"
done
