#!/usr/bin/env python3

import httpx
import yaml
from pathlib import Path
import logging
import os
import sys

sys.path.append(str(Path(__file__).parent))
import get_from_zenodo

sys.path.append(str(Path(__file__).parent.parent / "test"))
import _util as util


def find_fm_file(path: Path, tool: str) -> str:
    tool_yml = path / "data" / f"{tool}.yml"
    if tool_yml.exists():
        return tool_yml
    raise AssertionError(f"Could not find yml for tool {tool} in {path}.")


def download_tool(tool: str, doi: str, archives_root: Path, competition_track: str):
    logging.info(f"Updating '{tool}' for track '{competition_track}' ({doi})")
    zenodo_id = doi.replace("10.5281/zenodo.", "")
    cache_path = archives_root / f"cache/{zenodo_id}.zip"
    tool_path = (
        archives_root / f"{tool}-{util.get_track_for_filename(competition_track)}.zip"
    )
    client = httpx.Client(http2=True)
    if not tool_path.absolute().is_symlink():
        if not cache_path.is_file():
            get_from_zenodo.retry_download(
                client, doi, "application/zip", cache_path, 3
            )
        os.symlink(
            os.path.relpath(cache_path, start=archives_root),
            tool_path.absolute(),
        )
    else:
        real_path = tool_path.resolve()
        current_doi = os.path.basename(str(real_path)).replace(".zip", "")
        if current_doi != zenodo_id:
            tool_path.unlink()
            # Do not remove cached archive, because it might be used by other versions, or even other tools.
            # real_path.unlink()
            get_from_zenodo.retry_download(
                client, doi, "application/zip", cache_path, 3
            )
            os.symlink(
                os.path.relpath(cache_path, start=archives_root),
                tool_path.absolute(),
            )
        else:
            logging.info(f"Archive with DOI {doi} already exists, skipping.")


def parse_arguments():
    import argparse

    path_this_file = os.path.dirname(os.path.realpath(__file__))
    parser = argparse.ArgumentParser(description="Download an archive from Zenodo")
    parser.add_argument(
        "--fm-root",
        type=Path,
        help="Path to root of formal-methods repository",
    )
    parser.add_argument(
        "--archives-root",
        type=Path,
        help="Path to root of archives repository",
    )
    parser.add_argument(
        "--competition",
        type=str,
        help="Competition",
    )
    parser.add_argument(
        "--competition-track",
        type=str,
        help="Competition track",
    )
    parser.add_argument(
        "TOOL",
        nargs=1,
        type=str,
        default="",
        help="TOOL to download",
    )
    return parser.parse_args()


def main():
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
    args = parse_arguments()
    fm_yml = find_fm_file(args.fm_root, args.TOOL[0])
    with open(fm_yml) as f:
        tool_data = yaml.load(f, Loader=yaml.FullLoader)
    tool_version = ""
    for participation in tool_data["competition_participations"]:
        if (
            participation["competition"] == args.competition
            and participation["track"] == args.competition_track
        ):
            tool_version = participation["tool_version"]
    assert tool_version != ""
    doi = ""
    for version in tool_data["versions"]:
        if version["version"] == tool_version:
            doi = version["doi"]
    assert doi.startswith("10.5281/zenodo.")
    download_tool(args.TOOL[0], doi, args.archives_root, args.competition_track)


if __name__ == "__main__":
    main()
