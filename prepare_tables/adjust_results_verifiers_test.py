# This file is part of BenchExec, a framework for reliable benchmarking:
# https://github.com/sosy-lab/benchexec
#
# SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import copy
import os.path
import sys
import unittest
import xml.etree.ElementTree as ET  # noqa: What's wrong with ET?

from prepare_tables import adjust_results_verifiers
from benchexec import result
from benchexec import tablegenerator

sys.dont_write_bytecode = True  # prevent creation of .pyc files

test_data = {
    "verifier_correct": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "verifier_correct.xml",
    ),
    "verifier_wrong": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "verifier_wrong.xml",
    ),
    "verifier_no_data": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "verifier_no_data.xml",
    ),
    "verifier_error": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "verifier_error.xml",
    ),
    "validator_confirm": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "validator_confirm.xml",
    ),
    "validator_reject": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "validator_reject.xml",
    ),
    "validator_timeout": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "validator_timeout.xml",
    ),
    "validator_out_of_memory": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "validator_out_of_memory.xml",
    ),
    "validator_no_data": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "validator_no_data.xml",
    ),
    "linter_done": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "linter_done.xml",
    ),
    "linter_error": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "linter_error.xml",
    ),
    "linter_no_data": os.path.join(
        os.path.dirname(__file__),
        "test_adjust_results_verifiers/test_data",
        "linter_no_data.xml",
    ),
}

results_xml = ET.parse(  # noqa S314, the XML is trusted
    os.path.join(
        os.path.dirname(__file__), "test_adjust_results_verifiers/mock_results.xml"
    )
).getroot()
witness_xml_1 = ET.parse(  # noqa S314, the XML is trusted
    os.path.join(
        os.path.dirname(__file__), "test_adjust_results_verifiers/mock_witness_1.xml"
    )
).getroot()
witness_xml_2 = ET.parse(  # noqa S314, the XML is trusted
    os.path.join(
        os.path.dirname(__file__), "test_adjust_results_verifiers/mock_witness_2.xml"
    )
).getroot()

files = [
    "../sv-benchmarks/c/array-examples/sanfoundry_24-1.yml",
    "../sv-benchmarks/c/array-examples/data_structures_set_multi_proc_trivial_ground.yml",
    "../sv-benchmarks/c/array-patterns/array28_pattern.yml",
    "../sv-benchmarks/c/reducercommutativity/rangesum05.yml",
    "../sv-benchmarks/c/array-fpi/indp4f.yml",
]


def mock_witness_sets():
    witness_sets = {}
    for witness in [witness_xml_1, witness_xml_2]:
        for run in witness.findall("run"):
            name = run.get("name")
            witness_sets[name] = run
    return [witness_sets]


def mock_get_verification_result(name):
    return results_xml.find(f"run[@name='{name}']")


def mock_get_witness(name):
    witness = mock_witness_sets()[0].get(name)
    if witness is None:
        raise NotImplementedError(name)
    return witness


def element_trees_equal(et1, et2):
    if len(et1) != len(et2) or et1.tag != et2.tag or et1.attrib != et2.attrib:
        return False
    return all(element_trees_equal(child1, child2) for child1, child2 in zip(et1, et2))


def prepare_files(test_case):
    result_file = os.path.join("test_adjust_results_verifiers", test_case["verifier"])
    witness_files = list(
        map(
            lambda prepend: os.path.join("test_adjust_results_verifiers", prepend),
            test_case["validators"] + [test_case["linter"]],
        )
    )
    result_xml = tablegenerator.parse_results_file(result_file)
    witness_sets = []
    for witnessFile in witness_files:
        if not os.path.exists(witnessFile) or not os.path.isfile(witnessFile):
            sys.exit(f"File {witnessFile!r} does not exist.")
        witness_xml = tablegenerator.parse_results_file(witnessFile)
        witness_sets.append(adjust_results_verifiers.get_witnesses(witness_xml))

    adjust_results_verifiers.merge(result_xml, witness_sets, True)
    return result_xml


class TestMergeBenchmarkSets(unittest.TestCase):
    def test_only_elem(self):
        new_results = adjust_results_verifiers.xml_to_string(results_xml)
        new_witness_1 = adjust_results_verifiers.xml_to_string(witness_xml_1)
        new_witness_2 = adjust_results_verifiers.xml_to_string(witness_xml_2)
        self.assertTrue(
            element_trees_equal(
                ET.fromstring(new_results), results_xml  # noqa S314, the XML is trusted
            )
        )
        self.assertTrue(
            element_trees_equal(
                ET.fromstring(new_witness_1),  # noqa S314, the XML is trusted
                witness_xml_1,
            )
        )
        self.assertTrue(
            element_trees_equal(
                ET.fromstring(new_witness_2),  # noqa S314, the XML is trusted
                witness_xml_2,
            )
        )

    def test_set_doctype(self):
        qualified_name = "result"
        public_id = "+//IDN sosy-lab.org//DTD BenchExec result 1.18//EN"
        system_id = "https://www.sosy-lab.org/benchexec/result-1.18.dtd"
        new_results = adjust_results_verifiers.xml_to_string(
            results_xml, qualified_name, public_id, system_id
        )
        new_witness_1 = adjust_results_verifiers.xml_to_string(
            witness_xml_1, qualified_name, public_id, system_id
        )
        new_witness_2 = adjust_results_verifiers.xml_to_string(
            witness_xml_2, qualified_name, public_id, system_id
        )
        self.assertTrue(
            element_trees_equal(
                results_xml, ET.fromstring(new_results)  # noqa S314, the XML is trusted
            )
        )
        self.assertTrue(
            element_trees_equal(
                witness_xml_1,
                ET.fromstring(new_witness_1),  # noqa S314, the XML is trusted
            )
        )
        self.assertTrue(
            element_trees_equal(
                witness_xml_2,
                ET.fromstring(new_witness_2),  # noqa S314, the XML is trusted
            )
        )
        for xml in [new_results, new_witness_1, new_witness_2]:
            self.assertListEqual(
                [line.strip() for line in xml.splitlines()[1:4]],
                [
                    f"<!DOCTYPE {qualified_name}",
                    f"PUBLIC '{public_id}'",
                    f"'{system_id}'>",
                ],
            )

    def test_getWitnesses(self):
        witness1 = adjust_results_verifiers.get_witnesses(witness_xml_1)
        witness2 = adjust_results_verifiers.get_witnesses(witness_xml_2)
        self.assertEqual(3, len(witness1))
        self.assertEqual(2, len(witness2))
        self.assertSetEqual(
            {
                "../sv-benchmarks/c/array-examples/sanfoundry_24-1.yml",
                "../sv-benchmarks/c/array-examples/data_structures_set_multi_proc_trivial_ground.yml",
                "../sv-benchmarks/c/array-patterns/array28_pattern.yml",
            },
            set(witness1.keys()),
        )
        self.assertSetEqual(
            {
                "../sv-benchmarks/c/reducercommutativity/rangesum05.yml",
                "../sv-benchmarks/c/array-fpi/indp4f.yml",
            },
            set(witness2.keys()),
        )

    def test_getWitnessResult_no_witness(self):
        self.assertEqual(
            ("validation run missing", result.CATEGORY_ERROR),
            adjust_results_verifiers.get_witness_result(None, None),
        )
        self.assertEqual(
            ("validation run missing", result.CATEGORY_ERROR),
            adjust_results_verifiers.get_witness_result(None, results_xml.find("run")),
        )

    def test_getWitnessResult_no_verification_result(self):
        for file in files[:-1]:
            tuple_result = adjust_results_verifiers.get_witness_result(
                mock_get_witness(file), None
            )
            self.assertTrue(
                ("result invalid (not found)", result.CATEGORY_ERROR) == tuple_result
                or ("witness invalid (not found)", result.CATEGORY_ERROR)
                == tuple_result
            )
        self.assertEqual(
            ("witness invalid (not found)", result.CATEGORY_ERROR),
            adjust_results_verifiers.get_witness_result(
                mock_get_witness(files[-1]), None
            ),
        )

    def test_getWitnessResult(self):
        expected_results = [
            ("true", result.CATEGORY_CORRECT_UNCONFIRMED),
            ("result invalid (TIMEOUT)", result.CATEGORY_ERROR),
            ("result invalid (false(unreach-call))", result.CATEGORY_ERROR),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            ("witness invalid (false(unreach-call))", result.CATEGORY_ERROR),
        ]
        for expected, file in zip(expected_results, files):
            self.assertEqual(
                expected,
                adjust_results_verifiers.get_witness_result(
                    mock_get_witness(file), mock_get_verification_result(file)
                ),
            )

    def test_getValidationResult_single_witness(self):
        expected_results = [
            ("true", result.CATEGORY_CORRECT_UNCONFIRMED),
            ("result invalid (TIMEOUT)", result.CATEGORY_ERROR),
            ("result invalid (false(unreach-call))", result.CATEGORY_ERROR),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            ("witness invalid (false(unreach-call))", result.CATEGORY_ERROR),
        ]
        for expected, file in zip(expected_results, files):
            run = mock_get_verification_result(file)
            status_from_verification = run.find('column[@title="status"]').get("value")
            category_from_verification = run.find('column[@title="category"]').get(
                "value"
            )
            actual = adjust_results_verifiers.get_validation_result(
                run,
                mock_witness_sets(),
                status_from_verification,
                category_from_verification,
            )
            self.assertEqual(expected, actual[:2])
            self.assertEqual(
                (status_from_verification, category_from_verification), actual[2:]
            )

    def test_getValidationResult_multiple_witnesses(self):
        new_witness_results = [
            ("ERROR (invalid witness syntax)", result.CATEGORY_ERROR),
            ("ERROR (invalid witness file)", result.CATEGORY_ERROR),
            ("false (unreach-call)", result.CATEGORY_WRONG),
            ("true", result.CATEGORY_WRONG),
            ("false (unreach-call)", result.CATEGORY_CORRECT),
        ]
        expected_results = [
            ("witness invalid (true)", result.CATEGORY_ERROR),
            ("result invalid (TIMEOUT)", result.CATEGORY_ERROR),
            ("result invalid (false(unreach-call))", result.CATEGORY_ERROR),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            ("witness invalid (false(unreach-call))", result.CATEGORY_ERROR),
        ]
        witness_set_1 = mock_witness_sets()
        witness_set_2 = copy.deepcopy(witness_set_1)
        for expected, file, new_witness_result in zip(
            expected_results, files, new_witness_results
        ):
            verification_run = mock_get_verification_result(file)
            witness_run = witness_set_2[0].get(file)
            witness_run.find('column[@title="status"]').set(
                "value", new_witness_result[0]
            )
            witness_run.find('column[@title="category"]').set(
                "value", new_witness_result[1]
            )
            status_from_verification = verification_run.find(
                'column[@title="status"]'
            ).get("value")
            category_from_verification = verification_run.find(
                'column[@title="category"]'
            ).get("value")
            actual = adjust_results_verifiers.get_validation_result(
                verification_run,
                witness_set_1 + [{file: witness_run}],
                status_from_verification,
                category_from_verification,
            )
            self.assertEqual(expected, actual[:2])
            self.assertEqual(
                (status_from_verification, category_from_verification), actual[2:]
            )

    def test_getValidationResult_coverage_error_call(self):
        expected_results = [
            (None, None),
            (None, None),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            (None, None),
            (None, None),
        ]
        for expected, file in zip(expected_results, files):
            run = copy.deepcopy(mock_get_verification_result(file))
            run.set("properties", "coverage-error-call")
            status_from_verification = run.find('column[@title="status"]').get("value")
            category_from_verification = run.find('column[@title="category"]').get(
                "value"
            )
            actual = adjust_results_verifiers.get_validation_result(
                run,
                mock_witness_sets(),
                status_from_verification,
                category_from_verification,
            )
            self.assertEqual(expected, actual[:2])
            self.assertEqual(status_from_verification, actual[2])
            if file == "../sv-benchmarks/c/array-patterns/array28_pattern.yml":
                self.assertEqual(result.CATEGORY_CORRECT, actual[3])
                self.assertNotEqual(None, run.find('column[@title="score"]'))
            else:
                self.assertEqual(category_from_verification, actual[3])

    def test_getValidationResult_coverage_branches(self):
        for file in files:
            run = copy.deepcopy(mock_get_verification_result(file))
            run.set("properties", "coverage-branches")
            status_from_verification = run.find('column[@title="status"]').get("value")
            category_from_verification = run.find('column[@title="category"]').get(
                "value"
            )
            actual = adjust_results_verifiers.get_validation_result(
                run,
                mock_witness_sets(),
                status_from_verification,
                category_from_verification,
            )
            self.assertTupleEqual(
                (
                    status_from_verification,
                    result.CATEGORY_CORRECT,
                    status_from_verification,
                    result.CATEGORY_CORRECT,
                ),
                actual,
            )
            self.assertNotEqual(None, run.find('column[@title="score"]'))

    def test_getValidationResult_malformed_coverage(self):
        modified_run = copy.deepcopy(
            results_xml.find(
                'run[@name="../sv-benchmarks/c/array-examples/sanfoundry_24-1.yml"]'
            )
        )
        modified_run.set("properties", "coverage-branches")
        modified_witness_run = copy.deepcopy(
            witness_xml_1.find(
                'run[@name="../sv-benchmarks/c/array-examples/sanfoundry_24-1.yml"]'
            )
        )
        coverage_column = ET.Element(
            "column",
            title="branches_covered",
            value="fifty percent",  # this cannot be parsed into a number
        )
        modified_witness_run.append(coverage_column)
        actual = adjust_results_verifiers.get_validation_result(
            modified_run,
            [{modified_witness_run.get("name"): modified_witness_run}],
            result.RESULT_TRUE_PROP,
            result.CATEGORY_CORRECT,
        )
        # we should still be able to assign the correct results:
        self.assertTupleEqual(
            (
                result.RESULT_TRUE_PROP,
                result.CATEGORY_CORRECT,
                result.RESULT_TRUE_PROP,
                result.CATEGORY_CORRECT,
            ),
            actual,
        )
        # score should be None since we were not able to parse "fifty percent" above:
        self.assertTrue(modified_witness_run.find('column[@title="score"]') is None)

    def test_merge_no_witness(self):
        results_xml_cp1 = copy.deepcopy(results_xml)
        results_xml_cp2 = copy.deepcopy(results_xml)
        adjust_results_verifiers.merge(results_xml_cp2, [], True)
        for run in results_xml_cp1.findall("run"):
            del run.attrib["logfile"]
        self.assertEqual(ET.tostring(results_xml_cp1), ET.tostring(results_xml_cp2))

    def test_merge(self):
        expected_results = [
            ("true", result.CATEGORY_CORRECT_UNCONFIRMED),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            ("TIMEOUT", result.CATEGORY_ERROR),
            ("witness invalid (false(unreach-call))", result.CATEGORY_ERROR),
            ("false(unreach-call)", result.CATEGORY_WRONG),
        ]
        results_xml_cp = copy.deepcopy(results_xml)
        adjust_results_verifiers.merge(results_xml_cp, mock_witness_sets(), True)
        for expected, run in zip(expected_results, results_xml_cp.findall("run")):
            status = run.find('column[@title="status"]').get("value")
            category = run.find('column[@title="category"]').get("value")
            self.assertTupleEqual(expected, (status, category))

    def test_merge_no_overwrite(self):
        expected_results = [
            ("true", result.CATEGORY_CORRECT),
            ("false(unreach-call)", result.CATEGORY_CORRECT),
            ("TIMEOUT", result.CATEGORY_ERROR),
            ("witness invalid (false(unreach-call))", result.CATEGORY_ERROR),
            ("false(unreach-call)", result.CATEGORY_WRONG),
        ]
        results_xml_cp = copy.deepcopy(results_xml)
        adjust_results_verifiers.merge(results_xml_cp, mock_witness_sets(), False)
        for expected, run in zip(expected_results, results_xml_cp.findall("run")):
            status = run.find('column[@title="status"]').get("value")
            category = run.find('column[@title="category"]').get("value")
            self.assertTupleEqual(expected, (status, category))

    def test_merge_no_status_no_category(self):
        expected_results = [("not found", result.CATEGORY_CORRECT)] * 5
        modified_results = copy.deepcopy(results_xml)
        for run in modified_results.findall("run"):
            status_column = run.find('column[@title="status"]')
            category_column = run.find('column[@title="category"]')
            run.remove(status_column)
            run.remove(category_column)
            run.set("properties", "coverage-branches")
        adjust_results_verifiers.merge(modified_results, mock_witness_sets(), True)
        for expected, run in zip(expected_results, modified_results.findall("run")):
            status = run.find('column[@title="status"]').get("value")
            category = run.find('column[@title="category"]').get("value")
            self.assertTupleEqual(expected, (status, category))

    def test_merge_verifier_correct_linter_done_validator_timeout_memory_none(self):
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_no_data"],
                test_data["validator_out_of_memory"],
            ],
            "verifier": test_data["verifier_correct"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertNotEqual(
                result.CATEGORY_CORRECT,
                elem.find('column[@title="category"]').get("value"),
            )
            self.assertNotEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_correct_linter_done_validator_timeout_reject_none(self):
        # if no validator confirms a linter-approved "correct" witness, the category changes to "error/unknown"
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_reject"],
                test_data["validator_no_data"],
            ],
            "verifier": test_data["verifier_correct"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertNotEqual(
                result.CATEGORY_CORRECT,
                elem.find('column[@title="category"]').get("value"),
            )
            self.assertNotEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_correct_linter_done_validator_timeout_confirm_no_data(self):
        # if at least one validator confirms a linter-approved witness, the category stays correct
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_confirm"],
                test_data["validator_no_data"],
            ],
            "verifier": test_data["verifier_correct"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_CORRECT,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_correct_linter_done_validator_timeout_confirm_reject(self):
        # if at least one validator confirms a linter-approved witness, the category stays correct
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_confirm"],
                test_data["validator_reject"],
            ],
            "verifier": test_data["verifier_correct"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_CORRECT,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_wrong_linter_done_validator_timeout_reject_none(self):
        # if the verifier is wrong, the category must not change
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_reject"],
                test_data["validator_no_data"],
            ],
            "verifier": test_data["verifier_wrong"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_wrong_linter_done_validator_timeout_confirm_no_data(self):
        # if the verifier is wrong, the category must not change
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_confirm"],
                test_data["validator_no_data"],
            ],
            "verifier": test_data["verifier_wrong"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_wrong_linter_done_validator_timeout_confirm_reject(self):
        # if the verifier is wrong, the category must not change
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_confirm"],
                test_data["validator_reject"],
            ],
            "verifier": test_data["verifier_wrong"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_verifier_wrong_linter_done_validator_timeout_memory_none(self):
        # if the verifier is wrong, the category must not change
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_no_data"],
                test_data["validator_out_of_memory"],
            ],
            "verifier": test_data["verifier_wrong"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_WRONG,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_validators_timeout(self):
        test_input = {
            "validators": [
                test_data["validator_timeout"],
                test_data["validator_timeout"],
                test_data["validator_timeout"],
            ],
            "verifier": test_data["verifier_correct"],
            "linter": test_data["linter_done"],
        }
        for elem in prepare_files(test_input).findall("run"):
            self.assertEqual(
                result.CATEGORY_CORRECT_UNCONFIRMED,
                elem.find('column[@title="category"]').get("value"),
            )

    def test_merge_linter_error_or_no_data(self):
        # In any combination, the status has to be error if the linter has no file or detects errors.
        # However, if the verifier is wrong, the category stays wrong.
        for verifier in [
            "verifier_correct",
            "verifier_wrong",
            "verifier_error",
            "verifier_no_data",
        ]:
            for validator in [
                "validator_confirm",
                "validator_reject",
                "validator_timeout",
                "validator_no_data",
            ]:
                for linter in ["linter_error", "linter_no_data"]:
                    test_input = {
                        "validators": [test_data[validator]],
                        "verifier": test_data[verifier],
                        "linter": test_data[linter],
                    }
                    for elem in prepare_files(test_input).findall("run"):
                        if verifier == "verifier_wrong":
                            self.assertEqual(
                                result.CATEGORY_WRONG,
                                elem.find('column[@title="category"]').get("value"),
                            )
                        else:
                            self.assertEqual(
                                result.CATEGORY_ERROR,
                                elem.find('column[@title="category"]').get("value"),
                            )
