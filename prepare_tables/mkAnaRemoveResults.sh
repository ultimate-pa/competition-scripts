#!/bin/bash

source "$(dirname "$0")/../configure.sh"

# Number of processes running in parallel
NUMBER_JOBS=$(nproc)

find results-verified results-validated -name "*.xml.bz2" -print0 | xargs --null --max-args=1 --max-procs="$NUMBER_JOBS" \
    nice "$(dirname "$0")"/mkAnaRemoveResults.py --banned "$(dirname "$0")"/mkAnaRemoveResults.csv --resultsXML

