#!/usr/bin/env python3

# Generate a table definition for BenchExec's table generator.
# ./generate_table_def.py > table_all.xml
# ../benchexec/bin/table-generator --no-diff --format html --xml table_all.xml

import sys
import os
import yaml
import coloredlogs
import glob
import logging
import shlex
import subprocess
import functools
from multiprocessing import Pool

import utils


def get_verifiers_union(cat_def, validator_subcategory) -> tuple[str, str]:
    validator, subcategory = validator_subcategory
    year = str(cat_def["year"])[-2:]
    verifiers_union = ""
    for verifier in cat_def["verifiers"]:
        if cat_def["verifiers"][verifier]["lang"] != "C":
            continue
        result_file = utils.find_latest_file(
            validator,
            verifier,
            subcategory,
            year=year,
            fixed=True,
        )
        if not result_file:
            continue
        correctness_linter_files = utils.find_latest_file(
            "witnesslint-validate-correctness-witnesses",
            verifier,
            subcategory,
            year=year,
            fixed=True,
        )
        assert (
            correctness_linter_files
        ), f"WitnessLint results missing for correctness witnesses for verifier {verifier} and category {subcategory}."
        violation_linter_files = utils.find_latest_file(
            "witnesslint-validate-violation-witnesses",
            verifier,
            subcategory,
            year=year,
            fixed=True,
        )
        assert (
            violation_linter_files
        ), f"WitnessLint results missing for violation witnesses for verifier {verifier} and category {subcategory}."

        if os.path.exists(result_file):
            verifiers_union += f'    <result id="{verifier}"  filename="{os.path.basename(result_file)}"/>\n'
    return validator_subcategory, verifiers_union


def generate_table_def():
    with open("benchmark-defs/category-structure.yml") as f:
        cat_def = yaml.load(f, Loader=yaml.Loader)

    logging.info("Creating table-definition entries ...")
    subcategories = []
    for category in cat_def["categories"]:
        if "Overall" in category:
            continue
        subcategories += cat_def["categories"][category]["categories"]
    worklist = (
        (validator, subcategory)
        for validator in cat_def["validators"]
        for subcategory in subcategories
    )
    get_verifiers_union_partial = functools.partial(get_verifiers_union, cat_def)
    with Pool(processes=os.cpu_count()) as p:
        verifier_unions = dict(p.map(get_verifiers_union_partial, worklist))

    logging.info("Writing table definition files ...")
    header = (
        '<?xml version="1.0" ?>\n'
        + '<!DOCTYPE table PUBLIC "+//IDN sosy-lab.org//DTD BenchExec table 1.0//EN" "http://www.sosy-lab.org/benchexec/table-1.0.dtd">\n'
        + "<table>\n"
        + '  <column title="status"                         displayTitle="Status"/>\n'
        + '  <column title="score"                          displayTitle="Raw Score"/>\n'
        + '  <column title="witnesslint-witness-type"       displayTitle="Witness Type"/>\n'
        + '  <column title="cputime"     numberOfDigits="2" displayTitle="CPU"/>\n'
        + '  <column title="memory"      numberOfDigits="2" displayTitle="Mem"     displayUnit="MB" sourceUnit="B"/>\n'
    )
    year = str(cat_def["year"])[-2:]
    competition = str(cat_def["competition"])
    validation_kinds = [
        "validate-correctness-witnesses",
        "validate-violation-witnesses",
    ]
    tables_sub = dict()
    for subcategory in cat_def["categories_table_order"]:
        if "Overall" in subcategory:
            continue
        for kind in validation_kinds:
            tables_sub[f"{kind}.{subcategory}"] = open(
                f"results-validated/{kind}.results.{competition}{year}_{subcategory}.xml",
                "w",
            )
            tables_sub[f"{kind}.{subcategory}"].write(header + "\n")

    for validator in cat_def["validators"]:
        if cat_def["validators"][validator]["lang"] != "C":
            continue
        table_val = open(
            f"results-validated/{validator}.results.{competition}{year}.xml", "w"
        )
        table_val.write(header + "\n")
        table_val.write(f'  <union title="{validator}">\n')
        for category in cat_def["categories"]:
            if "Overall" in category:
                continue
            table_val_prop = open(
                f"results-validated/{validator}.results.{competition}{year}_{category}.xml",
                "w",
            )
            table_val_prop.write(header + "\n")
            table_val_prop.write(f'  <union title="{validator}_{category}">\n')
            # Tables for categories over all validators
            for kind in validation_kinds:
                if kind in validator or "witnesslint" in validator:
                    tables_sub[f"{kind}.{category}"].write(
                        f'  <union title="{validator}_{category}">\n'
                    )

            for subcategory in cat_def["categories"][category]["categories"]:
                for kind in validation_kinds:
                    if kind in validator or "witnesslint" in validator:
                        tables_sub[f"{kind}.{category}"].write(
                            f"    <!-- {category}.{subcategory} -->\n"
                        )
                table_val_prop.write(f"    <!-- {category}.{subcategory} -->\n")
                # Tables for subcategories
                table_val_prop_sub = open(
                    f"results-validated/{validator}.results.{competition}{year}_{subcategory}.xml",
                    "w",
                )
                table_val_prop_sub.write(header + "\n")
                table_val_prop_sub.write(
                    f'  <union title="{validator}_{subcategory}">\n'
                )
                # Tables for subcategories over all validators
                for kind in validation_kinds:
                    if kind in validator or "witnesslint" in validator:
                        tables_sub[f"{kind}.{subcategory}"].write(
                            f'  <union title="{validator}_{subcategory}">\n'
                        )

                table_val.write(verifier_unions[validator, subcategory])
                table_val_prop.write(verifier_unions[validator, subcategory])
                table_val_prop_sub.write(verifier_unions[validator, subcategory])
                for kind in validation_kinds:
                    if kind in validator or "witnesslint" in validator:
                        tables_sub[f"{kind}.{category}"].write(
                            verifier_unions[validator, subcategory]
                        )
                        tables_sub[f"{kind}.{subcategory}"].write(
                            verifier_unions[validator, subcategory]
                        )

                for kind in validation_kinds:
                    if kind in validator or "witnesslint" in validator:
                        tables_sub[f"{kind}.{subcategory}"].write("  </union>\n")
                table_val_prop_sub.write("  </union>\n")
                table_val_prop_sub.write("</table>\n")
                table_val_prop_sub.close()
            for kind in validation_kinds:
                if kind in validator or "witnesslint" in validator:
                    tables_sub[f"{kind}.{category}"].write("  </union>\n")
            table_val_prop.write("  </union>\n")
            table_val_prop.write("</table>\n")
            table_val_prop.close()
        table_val.write("  </union>\n")
        table_val.write("</table>\n")
        table_val.close()
    for subtable in tables_sub:
        tables_sub[subtable].write("</table>\n")
        tables_sub[subtable].close()
    logging.info("Done creating table-definition files.")


def main():
    coloredlogs.install(fmt="%(levelname)s %(process)s %(name)s %(message)s")
    generate_table_def()


if __name__ == "__main__":
    sys.exit(main())
