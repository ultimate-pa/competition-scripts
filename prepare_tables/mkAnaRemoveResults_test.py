import unittest
import prepare_tables.mkAnaRemoveResults as delete
import pathlib
import os
from benchexec import tablegenerator


class TestMergeBenchmarkSets(unittest.TestCase):
    def test_remove_correct(self):
        banned = {"../sv-benchmarks/c/test.yml", "../sv-benchmarks/c/test/test.yml"}
        xml = os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "test_mkAnaRemoveResults/test_data",
            "test_run.xml.bz2",
        )
        xml_backup = os.path.join(
            pathlib.Path(__file__).parent.resolve(),
            "test_mkAnaRemoveResults/test_data",
            "test_run.xml.bz2.backup.xml.bz2",
        )
        delete.overwrite(xml, banned, safe=True)
        for run in tablegenerator.parse_results_file(xml_backup).findall("run"):
            for b in banned:
                self.assertFalse(run.get("name").endswith(b))
        self.assertEqual(
            len(tablegenerator.parse_results_file(xml_backup).findall("run")), 1
        )
        os.remove(xml_backup)
