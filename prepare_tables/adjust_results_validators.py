#!/usr/bin/env python3

# Fix status in results XML.

import sys
import os
import yaml
import argparse
import logging
import coloredlogs
from xml.etree import ElementTree
from multiprocessing import Pool
from benchexec import tablegenerator
from benchexec import result

import utils


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "resultsXML",
        nargs="?",
        metavar="results-xml",
        help="XML file containing the results",
    )
    parser.add_argument(
        "violationLinterXML",
        nargs="?",
        metavar="violation-linter-xml",
        help="XML file containing validation results",
    )
    parser.add_argument(
        "correctnessLinterXML",
        nargs="?",
        metavar="correctness-linter-xml",
        help="XML file containing correctness results",
    )
    return parser.parse_args(argv)


def map_results(results_xml):
    return {run.get("name"): run for run in results_xml.findall("run")}


def generate_map(violation_linter_file, correctness_linter_file):
    m1 = map_results(violation_linter_file)
    m2 = map_results(correctness_linter_file)
    intersect = set(m1.keys()).intersection(set(m2.keys()))
    if intersect:
        with open("err.log", "a+") as fp:
            fp.write(str((intersect, violation_linter_file, correctness_linter_file)))
        logging.error(
            "Violation witness file and correctness witness file contain the same run? See 'err.log' for more information"
        )
    return m1 | m2


def adjust_results(result_file, violation_linter_file, correctness_linter_file):
    if not os.path.exists(result_file):
        logging.error(f"File {result_file!r} does not exist.")
    if not os.path.exists(violation_linter_file):
        logging.error(f"File {violation_linter_file!r} does not exist.")
    if not os.path.exists(correctness_linter_file):
        logging.error(f"File {correctness_linter_file!r} does not exist.")
    result_xml = tablegenerator.parse_results_file(result_file)
    is_witnesslint = result_xml.get("toolmodule") == "benchexec.tools.witnesslint"
    # Determine whether a validator for violation or correctness witnesses was executed
    validation_type = "violation_witness"
    if "validate-correctness-witnesses" in result_file:
        validation_type = "correctness_witness"

    violation_linter_xml = tablegenerator.parse_results_file(violation_linter_file)
    correctness_linter_xml = tablegenerator.parse_results_file(correctness_linter_file)
    linter_results = generate_map(violation_linter_xml, correctness_linter_xml)

    for run in result_xml.findall("run"):
        expected_result = run.get("expectedVerdict")
        status = run.find('column[@title="status"]').get("value")
        category = run.find('column[@title="category"]').get("value")
        if run.get("name") not in linter_results:
            continue
        linter_run = linter_results[run.get("name")]
        witness_type_column = linter_run.find(
            'column[@title="witnesslint-witness-type"]'
        )
        if witness_type_column is not None:
            witness_type = witness_type_column.get("value")

        # We add a field 'witness-category' to have the validity available for table-generator.
        linter_status = linter_run.find('column[@title="status"]').get("value")
        linter_category = linter_run.find('column[@title="category"]').get("value")
        if "witness does not exist" in linter_status:
            witness_category = result.WITNESS_CATEGORY_MISSING
            # Until BenchExec supresses executions with missing input files
            # (https://github.com/sosy-lab/benchexec/issues/785)
            # we shall drop runs for missing inputs.
            if not is_witnesslint:
                result_xml.remove(run)
                continue
        elif witness_type is not None and witness_type != validation_type:
            # Drop executions where a validator was executed for a wrong witness type
            result_xml.remove(run)
            continue
        elif "invalid witness syntax" in linter_status:
            witness_category = result.WITNESS_CATEGORY_ERROR
        elif "program does not exist" in linter_status:
            witness_category = result.WITNESS_CATEGORY_ERROR
        elif "EXCEPTION" in linter_status:
            witness_category = result.WITNESS_CATEGORY_ERROR
        elif (
            witness_type == "violation_witness" and expected_result.startswith("false")
        ) or (
            witness_type == "correctness_witness" and expected_result.startswith("true")
        ):
            witness_category = result.WITNESS_CATEGORY_CORRECT
        elif (
            witness_type == "violation_witness" and expected_result.startswith("true")
        ) or (
            witness_type == "correctness_witness"
            and expected_result.startswith("false")
        ):
            witness_category = result.WITNESS_CATEGORY_WRONG
        elif linter_category == result.CATEGORY_ERROR:
            # All error cases should be handled explicitely above.
            assert (
                False
            ), f'Unhandled ERROR case of witness category for task {run.get("name")}'
        elif linter_category == result.CATEGORY_UNKNOWN:
            # WitnessLint shall not produce unknown results,
            # rather we shall fix the result of WitnessLint (in the tool-info module)
            # to create a result that we understand.
            assert (
                False
            ), f'Unhandled UNKNOWN case of witness category for task {run.get("name")}'
        else:
            # Unexpected situation.
            assert (
                False
            ), f'Unhandled unexpected case of witness category for task {run.get("name")}'

        new_column = ElementTree.Element(
            "column",
            {
                "title": "witness-category",
                "value": witness_category,
            },
        )
        run.append(new_column)

        # The scoring schema is documented in Fig. 3 on page 171 in the paper:
        #   https://doi.org/10.1007/978-3-031-22308-2_8
        # and a table with all the cases is available in the MR:
        #   https://gitlab.com/sosy-lab/benchmarking/competition-scripts/-/merge_requests/57

        # We adjust the result category of the run.
        if category not in [
            result.CATEGORY_CORRECT,
            result.CATEGORY_WRONG,
        ]:
            # Categories derived from the validator result that we do not need to adjust,
            # such as ERROR and UNKNOWN.
            category_new = category
        elif witness_type_column is None:
            # We do not know the witness type.
            category_new = result.CATEGORY_UNKNOWN
        elif witness_category not in [
            result.WITNESS_CATEGORY_CORRECT,
            result.WITNESS_CATEGORY_WRONG,
        ]:
            # The witness is neither correct nor wrong.
            category_new = result.CATEGORY_UNKNOWN
        elif (
            witness_type == "violation_witness"
            and expected_result.startswith("false")
            and status.startswith("true")
        ) or (
            witness_type == "correctness_witness"
            and expected_result.startswith("true")
            and status.startswith("false")
        ):
            # We do not know if a valid* witness is refuted rightfully (cases 2 and 7 in above table).
            category_new = result.CATEGORY_UNKNOWN
        elif status.startswith(expected_result):
            category_new = result.CATEGORY_CORRECT
        elif (status.startswith("false") and expected_result.startswith("true")) or (
            status.startswith("true") and expected_result.startswith("false")
        ):
            category_new = result.CATEGORY_WRONG
        else:
            # Leave unchanged.
            category_new = category
        run.find('column[@title="category"]').set("value", category_new)

        if not is_witnesslint:
            for column in linter_run.findall("column"):
                if column.get("title").startswith("witnesslint-"):
                    new_column = ElementTree.Element(
                        "column",
                        {
                            "title": column.get("title"),
                            "value": column.get("value"),
                        },
                    )
                    run.append(new_column)
    fixed_file = result_file + ".fixed.xml.bz2"
    logging.info(f"   Writing file: {fixed_file}")
    utils.write_xml_file(fixed_file, result_xml)


# needed for thread pool
def wrap_fix(tpl):
    validator, verifier, subcategory, year = tpl
    result_file = utils.find_latest_file(validator, verifier, subcategory, year=year)
    if not result_file:
        logging.info(f"Missing result file for {tpl}.")
        return
    correctness_linter_file = utils.find_latest_file(
        "witnesslint-validate-correctness-witnesses",
        verifier,
        subcategory,
        year=year,
    )
    if not correctness_linter_file:
        logging.info(f"Missing correctness witnesslint file for {tpl}.")
        return
    violation_linter_file = utils.find_latest_file(
        "witnesslint-validate-violation-witnesses",
        verifier,
        subcategory,
        year=year,
    )
    if not violation_linter_file:
        logging.info(f"Missing violation witnesslint file for {tpl}.")
        return
    adjust_results(result_file, violation_linter_file, correctness_linter_file)
    logging.info(f"{tpl} processed successfully!")
    return


def main():
    coloredlogs.install(fmt="%(levelname)s %(process)s %(name)s %(message)s")
    args = parse_args(sys.argv[1:])
    result_file = args.resultsXML
    violation_linter_file = args.violationLinterXML
    correctness_linter_file = args.correctnessLinterXML

    if len(sys.argv[1:]) > 1:
        # Adjust only one file
        adjust_results(
            result_file,
            violation_linter_file,
            correctness_linter_file,
        )
        return

    # Adjust all files of category structure
    runs = []
    with open("benchmark-defs/category-structure.yml") as f:
        cat_def = yaml.load(f, Loader=yaml.Loader)
    year = str(cat_def["year"])[-2:]
    logging.info("Create runs...")
    for validator in cat_def["validators"]:
        if cat_def["validators"][validator]["lang"] != "C":
            continue
        for category in cat_def["categories"]:
            if "Overall" in category:
                continue
            for subcategory in cat_def["categories"][category]["categories"]:
                for verifier in cat_def["verifiers"]:
                    if cat_def["verifiers"][verifier]["lang"] != "C":
                        continue
                    runs.append((validator, verifier, subcategory, year))
    num_runs = len(runs)
    logging.info("Done creating %d runs.", num_runs)
    logging.info("Create fixed files...")
    with Pool(processes=os.cpu_count()) as p:
        p.map(wrap_fix, runs)
    logging.info("Done creating %d fixed files.", num_runs)


if __name__ == "__main__":
    sys.exit(main())
