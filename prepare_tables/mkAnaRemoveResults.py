#!/usr/bin/env python3

# This file is part of the competition environment.
#
# SPDX-FileCopyrightText: 2011-2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

"""
This script removes a given set of run results from an XML results file in BenchExec format.
"""

import argparse
import sys
import csv

from benchexec import tablegenerator

import utils


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--resultsXML",
        help="XML file containing the results",
    )
    parser.add_argument(
        "--banned",
        help="TXT file containing excluded tasks",
    )
    parser.add_argument(
        "--safe",
        help="store result in separate file",
        action=argparse.BooleanOptionalAction,
    )
    return parser.parse_args(argv)


def read_csv(banned_csv: str):
    result_set = set()
    with open(banned_csv, "r") as banned:
        lines = csv.reader(banned, delimiter="\n")
        for line in lines:
            result_set.add(*line)
    return result_set


def overwrite(xml: str, banned: set, safe=None):
    result_xml = tablegenerator.parse_results_file(xml)
    print("Processing", xml)
    for run in result_xml.findall("run"):
        if run.get("name") in banned:
            print("Remove", run.get("name"))
            result_xml.remove(run)
    output_path = xml if safe is None else f"{xml}.backup.xml.bz2"
    utils.write_xml_file(output_path, result_xml)
    print("Done.")


def main():
    args = parse_args(sys.argv[1:])
    result_file = args.resultsXML
    banned_csv = args.banned
    banned_set = read_csv(banned_csv)
    overwrite(result_file, banned_set, safe=args.safe)


if __name__ == "__main__":
    sys.exit(main())
